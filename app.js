var cfenv = require('cfenv');
var express = require('express');
var http = require('http');
var ws = require('ws');
var fs = require("fs");

// Environment
var environment = cfenv.getAppEnv();

// Web
var app = express();

// Static
app.use('/', express.static('public'));

// Sockets
var server = http.createServer();
var sockets = new ws.Server({
	server : server
});

// Listeners
sockets.on('connection', function(client) {
	var count = 0;
	var html;
	var js;
	var css;
	console.log('Connection Established.');

	// [command] [user] [password] [|] [ [html/js/css]+  [| documentData]+ ]
	client.on('message', function(message) {
		var words = message.split('|');
		var metadata = words[0].split(' ');
		var command = metadata[0];
		var userName = metadata[1];
		var password = metadata[2];
		
		//ex.
		//create bob123 abcde |
		if (command === 'login') {
			if (validateUser(userName, password) === 1) {
				console.log('validated ' + userName);
				client.send('success');
			} else {
				client.send('invalid username or password');
				return;
			}
		} else if (command === 'create') {
			console.log('creating user ' + userName + '...');
			var pass = createUser(userName, password);
			if (pass === 0) {
				client.send('user exists');
				console.log('user exists');
			} else {
				console.log('created user ' + userName);
				client.send('success');
			}
		//ex.
		//load bob123 abcde |
		} else {
			if (command === 'load') {
				if (validateUser(userName, password) !== 1) {
					client.send('invalid username or password');
					return;
				} else {
					files = getUserFiles(userName);
					console.log('validated ' + userName);
					html = files[0];
					js = files[1];
					css = files[2];
				}
			
				if (count === 0) {
					console.log('sending html file');
					count++;
					client.send(html);
				} else if (count === 1) {
					console.log('sending js file');
					count++;
					client.send(js);
				} else if (count === 2) {
					console.log('sending css file');
					count++;
					client.send(css);
				} else if (count === 3) {
					console.log('done sending files for client');
					count = 0;
				}
			//ex.
			//save bob123 abcde | js | [document data]
			} else if (command === 'save') {
				var documentData = words[2];
				var fileType = words[1];

				if (validateUser(userName, password) === 1) {
					console.log('saving ' + fileType + 'file for ' + userName);
					storeUserFiles(userName, documentData, fileType);
					client.send('success');
				}
			} 
		}
	});
});

// Start
server.on('request', app);
server.listen(environment.port, function() {
	console.log(environment.url);
});

function validateUser(userName, password) {
	var storedPassword = getFileData(userName + '/password.txt');
	
	fs = require('fs')
	/*fs.readFile(userName + '/password.txt', 'utf8', function (err,data) {
	  if (err) {
	    console.log(err);
		return 'error';
	  }
	  storedPassword = data;
	  console.log('file data: ' + data);
	});
	*/
	if (storedPassword !== password) {
		console.log('invalid credentials for ' + userName + '[' + storedPassword + ']' + '[' + password + ']');

		return 0;
	} else {
		return 1;
	}
}

function createDir(dirName) {
	var fs = require('fs');

	if (!fs.existsSync(dirName)) {
		console.log('created dir ' + dirName);
		fs.mkdirSync(dirName);
		
		return 1;
	} else {
		console.log('dir ' + dirName + ' exists');
		return 0;
	}
}

function storeUserFiles(userName, data, fileType) {
	var fs = require('fs');

	createDir(userName);
	fs.writeFile(userName + '/app.' + fileType, data, function(err) {
		if (err) {
			return console.log(err);
		}

		console.log("The file " + fileType + "was saved!");
	});
}

function getUserFiles(userName) {
	var htmlFile = getFileData(userName + '/app.html');
	var jsFile = getFileData(userName + '/app.js');
	var cssFile = getFileData(userName + '/app.css');
	
	var files = [
	            htmlFile,
	            jsFile,
	            cssFile
	        ];
	
	return files; passw
}

function getFileData(path) {
	fs = require('fs');
	var data;
	
	try {
		data = fs.readFileSync(path, 'utf8');
	}
	catch(err) {
	    return 'error';
	}
	
	return data;
}

function createUser(userName, password) {

	if (createDir(userName) == 0) {
		return 0;
	}

	fs.writeFile(userName + "/password.txt", password, function(err) {
		if (err) {
			console.log(err);
			return 0;
		}

		console.log("The password file was saved!");
		return 1;
	});
}
